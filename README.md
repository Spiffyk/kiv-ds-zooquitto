# KIV/DS - ZooQuitto - Distribuovaný message broker

**Autor:** Oto Šťáva (A20N0107P)

Tato semestrální práce obsahuje dva vlastní programy napsané v jazyce C: plugin pro *Eclipse Mosquitto* a MQTT klienta.


## Mosquitto plugin

Plugin se po svém spuštění pomocí dodaných knihoven připojí k jedné z běžících instancí Apache ZooKeeper, v níž vytvoří *persistentní* ZNode `/zooquitto` (pokud již neexistuje) a v něm *efemérní* a *sekvenční* ZNode `/zooquitto/zq<číslo>`, jehož obsahem je adresa aktuální instance Eclipse Mosquitto.

Po vytvoření své ZNode hledá další ZNodes, aby se mohl propojit s ostatními instancemi Eclipse Mosquitto.

Plugin odchytává veškeré zprávy, které přijdou na danou instanci Mosquitto a předává je všem ostatním instancím s prefixem `(fwd)` - tímto způsobem dochází k ošetření, aby již přeposlané zprávy nebyly donekonečna kaskádovitě rozesílány. Pro použití klienty plugin prefix `(fwd)` odstraňuje, tedy klienti vidí všechny zprávy bez prefixu.


## MQTT klient

Klient je jednoduchý program, který se připojí k jedné z běžících instancí Apache ZooKeeper a v ZNode `/zooquitto` náhodně vybere jeden z *efemérních* ZNodes. Z tohoto ZNode získá adresu instance Eclipse Mosquitto, ke které se pokusí připojit, a začne na ni posílat náhodně generované zprávy.

Při ztrátě spojení hledá klient v ZooKeeperu opět další instanci Mosquitto.


## Stažení projektu

Projekt využívá *Git Submodules* pro získání dependencí souvisejících s SDK aplikací *Apache ZooKeeper* a
*Ecplise Mosquitto*.

Pro naklonování repozitáře včetně submodulů lze využít následující příkaz:

```
$ git clone https://gitlab.com/Spiffyk/kiv-ds-zooquitto.git --recurse-submodules
```

Pokud máte repozitář již naklonovaný bez submodulů, lze všechny submoduly dodatečně získat následujícím příkazem:

```
$ git submodule update --init --recursive
```


## Sestavení a spuštění

Programy lze sestavit a spustit nástrojem Vagrant pomocí souboru `deploy/Vagrantfile`:

```
$ vagrant up
```

Tento příkaz sestaví potřebné obrazy kontejnerů a celý projekt spustí.

Funkčnost lze ověřit sledováním logů jednotlivých kontejnerů pomocí následujícího příkazu:

```
docker logs <název_kontejneru> --follow
```


## Výsledek

Aktuální implementace je v zásadě funkční, ale občas některé uzly z dosud neznámých důvodů spadnou či odmítají komunikovat. Jejich restart tento problém většinou řeší.

Z časových důvodů dosud nebyl implementován healthcheck.
