#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <mosquitto.h>
#include <mosquitto_broker.h>
#include <mosquitto_plugin.h>
#include <mqtt.h>
#include <zookeeper.h>

#include "../lib/utils.h"

#define SUPPORTED_MOSQUTTO_VERSION 5          /**< The version of Mosquitto plugin API supported by this plugin. */
#define ZOOKEEPER_HOSTS_INITIAL_CAPACITY 64   /**< The initial capacity of the `zooquitto::zk_hosts` string. */
#define MAX_PEERS 128
#define ZOOQUITTO_TOPIC "zooquitto"

static const char FWD_PREFIX[6] = "(fwd)";

/** Mosquitto log alias - Adds `[ZooQuitto]` before the log. */
#define zooquitto_log(level, fmt, ...) mosquitto_log_printf(level, "[ZooQuitto] " fmt __VA_OPT__(,) __VA_ARGS__)

/** MQTT client pool entry. */
struct peer {
    struct mqtt_client mqtt;
    char *host;
    int sock;
    uint8_t sendbuf[2048];
    uint8_t recvbuf[1024];
    _Atomic(bool) inited;
    _Atomic(bool) active;
};

/** Plugin state. */
struct zooquitto {
    mosquitto_plugin_id_t *plugin_id;   /**< ID of the plugin as assigned by Mosquitto. */
    char *my_host;                      /**< Hostname of this Mosquitto instance - retrieved via an env variable. */
    char *my_node;                      /**< ZNode name of this Mosquitto instance - retrieved during registration. */
    zhandle_t *zk;                      /**< ZooKeeper handle. */
    clientid_t zk_id;                   /**< ZooKeeper session ID. */
    pthread_t pinger;                   /**< Thread for pinging MQTT peers. */
    struct peer peers[MAX_PEERS];       /**< Pool of MQTT clients. */
    bool connected : 1;                 /**< `true` = connection to ZooKeeper is active. */
    bool pinger_active : 1;             /**< `true` = pinger thread is active */
};

void zooquitto_get_peers(struct zooquitto *zq); /* Forward decl */

/** Called on ZooKeeper peer state change. */
void zooquitto_peer_watcher(zhandle_t *zh, int type, int state, const char *path, void *context)
{
    struct zooquitto *zq = context;

    if (type == ZOO_CHILD_EVENT) {
        zooquitto_log(MOSQ_LOG_INFO, "Root node updated", path);
        zooquitto_get_peers(zq);
    }
}

void zooquitto_kill_mqtt(struct peer *p)
{
    if (p->inited && p->active) {
        mqtt_disconnect(&p->mqtt);
    }
    if (p->sock >= 0) {
        close(p->sock);
        p->sock = -1;
    }
    p->active = false;
}

void *zooquitto_pinger(void *data)
{
    struct zooquitto *zq = (struct zooquitto *) data;
    while (1) {
        for (size_t i = 0; i < MAX_PEERS; i++) {
            struct peer *p = &zq->peers[i];
            if (!p->inited || !p->active)
                continue;

            int ret = mqtt_sync(&p->mqtt);
            if (ret != MQTT_OK) {
                zooquitto_kill_mqtt(p);
            }
        }
        usleep(100000);
    }
    return NULL;
}

void zooquitto_publish_callback(void **state, struct mqtt_response_publish *publish)
{
    // Dummy callback - we don't receive message through MQTT-C here.
}

void zooquitto_single_peer_completion(int rc, const char *value, int value_len, const struct Stat *stat,
                                      const void *context)
{
    if (rc != ZOK) {
        zooquitto_log(MOSQ_LOG_ERR, "Peer node error: %s", zerror(rc));
        return;
    }

    struct zooquitto *zq = (struct zooquitto *) context;
    struct peer *target = NULL;
    char *host = strndup(value, value_len);

    if (strcmp(host, zq->my_host) == 0) {
        free(host);
        return;
    }

    for (size_t i = 0; i < MAX_PEERS; i++) {
        struct peer *p = &zq->peers[i];
        if (!p->inited || !p->active) {
            if (!target)
                target = p;
            continue;
        }

        if (strcmp(host, p->host) == 0) {
            free(host);
            host = p->host;
            target = p;
            break;
        }
    }

    if (!target) {
        zooquitto_log(MOSQ_LOG_ERR, "Too many peers detected!");
        free(host);
        return;
    }

    if (target->host && target->host != host) {
        free(target->host);
    }

    target->host = host;
    zooquitto_kill_mqtt(target);
    target->sock = open_nb_socket(target->host, "1883");
    if (target->sock < 0) {
        zooquitto_log(MOSQ_LOG_ERR, "Could not open MQTT socket (%s): %s", target->host, strerror(errno));
        return;
    }

    if (target->inited) {
        mqtt_reinit(&target->mqtt,
                  target->sock,
                  target->sendbuf, sizeof(target->sendbuf),
                  target->recvbuf, sizeof(target->recvbuf));
    } else {
        mqtt_init(&target->mqtt,
                  target->sock,
                  target->sendbuf, sizeof(target->sendbuf),
                  target->recvbuf, sizeof(target->recvbuf),
                  zooquitto_publish_callback);
        target->inited = true;
    }

    mqtt_connect(&target->mqtt, NULL, NULL, NULL, 0, NULL, NULL, MQTT_CONNECT_CLEAN_SESSION, 400);
    if (target->mqtt.error != MQTT_OK) {
        zooquitto_log(MOSQ_LOG_ERR, "MQTT init error: %s", mqtt_error_str(target->mqtt.error));
        zooquitto_kill_mqtt(target);
        return;
    }
    target->active = true;
}

/** Called by ZooKeeper when peers have been received. */
void zooquitto_peers_completion(int rc, const struct String_vector *peers, const void *context)
{
    char buf[512];
    struct zooquitto *zq = (struct zooquitto *) context;
    zooquitto_log(MOSQ_LOG_INFO, "Received nodes");
    for (int32_t i = 0; i < peers->count; i++) {
        bool is_me = (strcmp(peers->data[i], zq->my_node) == 0);

        zooquitto_log(MOSQ_LOG_INFO, "%" PRId32 ": %s%s", i + 1, peers->data[i],
                (is_me) ? " (that's me!)" : "");

        if (!is_me) {
            /* If this node is not mine, add it to peers */
            snprintf(buf, sizeof(buf), "/zooquitto/%s", peers->data[i]);
            int ret = zoo_aget(zq->zk, buf, false, zooquitto_single_peer_completion, zq);
            if (ret)
                zooquitto_log(MOSQ_LOG_INFO, "Could not request node %s: %s", buf, zerror(ret));
        }
    }
}

/** Retrieves peers from ZooKeeper. */
void zooquitto_get_peers(struct zooquitto *zq)
{
    int err = zoo_awget_children(zq->zk, "/zooquitto", zooquitto_peer_watcher, zq, zooquitto_peers_completion, zq);
    if (err)
        zooquitto_log(MOSQ_LOG_ERR, "Error watching zooquitto children: %s", zerror(err));
}

/** Called when this ZooQuitto instance completes adding its node into ZooKeeper. */
void zooquitto_reg_completion(int rc, const char *value, const void *context)
{
    if (rc != ZOK) {
        zooquitto_log(MOSQ_LOG_ERR, "Registration error: %s", zerror(rc));
        return;
    }

    struct zooquitto *zq = (struct zooquitto *) context;

    const char *name = strrchr(value, '/');
    if (name)
        name++;
    else
        name = value;
    zq->my_node = strdup(name);

    zooquitto_log(MOSQ_LOG_INFO, "Registered in ZooKeeper: %s", zq->my_node);
}

/** Called when this ZooQuitto instance completes adding the root ZooQuitto node into ZooKeeper. */
void zooquitto_root_node_completion(int rc, const char *value, const void *context)
{
    if (rc != ZOK && rc != ZNODEEXISTS) {
        zooquitto_log(MOSQ_LOG_ERR, "Could not create zooquitto root node: %s", zerror(rc));
        return;
    }

    int err;
    struct zooquitto *zq = (struct zooquitto *) context;

    err = zoo_acreate(zq->zk, "/zooquitto/zq", zq->my_host, strlen(zq->my_host),
            &ZOO_READ_ACL_UNSAFE, ZOO_EPHEMERAL_SEQUENTIAL,
            zooquitto_reg_completion, zq);
    if (err)
        zooquitto_log(MOSQ_LOG_ERR, "Error creating ephemeral node: %s", zerror(err));

    zooquitto_get_peers(zq);
}

/** Called on ZooKeeper session state change. */
void zooquitto_session_watcher(zhandle_t *zh, int type, int state, const char *path, void *context)
{
    struct zooquitto *zq = context;

    if (type == ZOO_SESSION_EVENT) {
        if (state == ZOO_CONNECTED_STATE) {
            zq->connected = true;
            zooquitto_log(MOSQ_LOG_WARNING, "Connected");

            const clientid_t *id = zoo_client_id(zh);
            if (zq->zk_id.client_id == 0 || zq->zk_id.client_id != id->client_id) {
                zq->zk_id = *id;
                zooquitto_log(MOSQ_LOG_INFO, "Got new session id: %" PRIx64, zq->zk_id.client_id);
            }

            int err = zoo_acreate(zh, "/zooquitto", NULL, 0,
                    &ZOO_OPEN_ACL_UNSAFE, ZOO_PERSISTENT,
                    zooquitto_root_node_completion, zq);
            if (err)
                zooquitto_log(MOSQ_LOG_ERR, "Error creating root node: %s", zerror(err));
        } else if (state == ZOO_NOTCONNECTED_STATE) {
            zq->connected = false;
            zooquitto_log(MOSQ_LOG_WARNING, "Not connected");
        } else if (state == ZOO_AUTH_FAILED_STATE) {
            zooquitto_log(MOSQ_LOG_ERR, "Authentication failure");
            zookeeper_close(zh);
            zq->zk = NULL;
        } else if (state == ZOO_EXPIRED_SESSION_STATE) {
            zooquitto_log(MOSQ_LOG_ERR, "Session expired");
            zookeeper_close(zh);
            zq->zk = NULL;
        }
    }
}

/** Called by Mosquitto when a message is received. */
int zooquitto_message_callback(int event_type, void *event_data, void *userdata)
{
    if (event_type != MOSQ_EVT_MESSAGE)
        return 0;

    struct zooquitto *zq = userdata;
    struct mosquitto_evt_message *evt = event_data;

    zooquitto_log(MOSQ_LOG_INFO, "RECV(%s): %s", evt->topic, evt->payload);
    if (evt->payloadlen >= strlen(FWD_PREFIX) && strncmp(FWD_PREFIX, evt->payload, strlen(FWD_PREFIX)) == 0) {
        /* Message has FWD_PREFIX - strip it and continue */
        size_t new_payloadlen = evt->payloadlen - strlen(FWD_PREFIX);
        char *new_payload = mosquitto_calloc(1, new_payloadlen);
        if (!new_payload)
            return MOSQ_ERR_NOMEM;
        strncpy(new_payload, &((char *) evt->payload)[strlen(FWD_PREFIX)], new_payloadlen);

        evt->payload = new_payload;
        evt->payloadlen = new_payloadlen;
    } else {
        /* Message does not have FWD_PREFIX - forward it */
        char *fwdmsg = NULL;
        asprintf(&fwdmsg, "%s%s", FWD_PREFIX, (char *) evt->payload);
        for (size_t i = 0; i < MAX_PEERS; i++) {
            struct peer *p = &zq->peers[i];
            if (!p->inited || !p->active)
                continue;

            mqtt_publish(&p->mqtt, evt->topic, fwdmsg, strlen(fwdmsg), 0);
        }
        free(fwdmsg);
    }
    return 0;
}


/* EXPORTED MOSQUITTO PLUGIN FUNCTIONS ********************************************************************************/

int mosquitto_plugin_version(int supported_version_count, const int *supported_versions)
{
    for (int i = 0; i < supported_version_count; i++) {
        if (supported_versions[i] == SUPPORTED_MOSQUTTO_VERSION)
            return SUPPORTED_MOSQUTTO_VERSION;
    }

    return -1;
}

int mosquitto_plugin_init(mosquitto_plugin_id_t *identifier,
                          void **userdata,
                          struct mosquitto_opt *options,
                          int option_count)
{
    /* Initialize state */
    struct zooquitto *zq = ch_malloc(sizeof(struct zooquitto));
    *zq = (struct zooquitto) {
        .plugin_id = identifier,
        .my_host = strdup(getenv("ZOOQUITTO_HOST")),
        .zk = NULL,
        .connected = false,
    };
    *userdata = zq;

    zq->zk = zookeeper_init(getenv("ZOOS"), zooquitto_session_watcher, 10000, 0, zq, 0);
    if (!zq->zk) {
        int err = errno;
        zooquitto_log(MOSQ_LOG_ERR, "ZooKeeper initialization error: %s", strerror(err));
        return err;
    } else {
        zooquitto_log(MOSQ_LOG_INFO, "ZooKeeper initialized");
    }

    int err = mosquitto_callback_register(zq->plugin_id, MOSQ_EVT_MESSAGE, zooquitto_message_callback, NULL, zq);
    if (err) {
        zooquitto_log(MOSQ_LOG_ERR, "Could not register message callback!");
        return err;
    }

    if (pthread_create(&zq->pinger, NULL, zooquitto_pinger, zq)) {
        zooquitto_log(MOSQ_LOG_ERR, "Could not start MQTT pinger");
        return 1;
    }
    zq->pinger_active = true;

    return 0;
}

int mosquitto_plugin_cleanup(void *userdata, struct mosquitto_opt *options, int option_count)
{
    struct zooquitto *zq = userdata;

    int err = mosquitto_callback_unregister(zq->plugin_id, MOSQ_EVT_MESSAGE, zooquitto_message_callback, NULL);
    if (err)
        zooquitto_log(MOSQ_LOG_ERR, "Could not unregister message callback!");

    if (zq->pinger_active)
        pthread_cancel(zq->pinger);

    if (zq->zk)
        zookeeper_close(zq->zk);

    /* TODO: free peer hosts */

    /* Free the state */
    free(zq->my_host);
    free(zq->my_node);
    free(zq);

    return 0;
}
