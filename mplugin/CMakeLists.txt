cmake_minimum_required(VERSION 3.7)
project(zooquitto)

set(CMAKE_C_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON) # For the plugin to be properly usable by Mosquitto
add_definitions(-D_GNU_SOURCE)

# Zookeeper client library from submodule
set(WITH_CYRUS_SASL OFF)
set(WITH_OPENSSL OFF)
set(WANT_CPPUNIT OFF)
add_definitions(-DTHREADED)
add_subdirectory(
    "${CMAKE_CURRENT_SOURCE_DIR}/../contrib/zookeeper/zookeeper-client/zookeeper-client-c"
    "${CMAKE_CURRENT_BINARY_DIR}/zookeeper-client"
)

# MQTT-C library from submodule
add_subdirectory(
    "${CMAKE_CURRENT_SOURCE_DIR}/../contrib/mqtt-c"
    "${CMAKE_CURRENT_BINARY_DIR}/mqtt-c"
)

# Main Mosquitto plugin target
add_library(zooquitto SHARED
    zooquitto.c
    ../lib/utils.c ../lib/utils.h
)
target_link_libraries(zooquitto
    PUBLIC zookeeper
    PUBLIC mqttc
)

# Include Mosquitto and ZooKeeper headers from submodule
target_include_directories(zooquitto SYSTEM PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/../contrib/mosquitto/include"
    "${CMAKE_CURRENT_SOURCE_DIR}/../contrib/zookeeper/zookeeper-client/zookeeper-client-c/include"
)

# Include zookeeper generated headers
target_include_directories(zooquitto PRIVATE
    "${CMAKE_CURRENT_SOURCE_DIR}/../contrib/zookeeper/zookeeper-client/zookeeper-client-c/generated"
)
