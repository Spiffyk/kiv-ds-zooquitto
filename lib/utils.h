#ifndef ZOOQUITTO_UTILS_H
#define ZOOQUITTO_UTILS_H

#include <stdlib.h>

/** malloc, but it aborts when it returns NULL. */
static inline void *ch_malloc(size_t size)
{
    void *obj = malloc(size);
    if (!obj)
        abort();
    return obj;
}

/** calloc, but it aborts when it returns NULL. */
static inline void *ch_calloc(size_t nmemb, size_t size)
{
    void *obj = calloc(nmemb, size);
    if (!obj)
        abort();
    return obj;
}

/** realloc, but it aborts when it returns NULL. */
static inline void *ch_realloc(void *ptr, size_t size)
{
    void *obj = realloc(ptr, size);
    if (!obj)
        abort();
    return obj;
}

/** Opens a non-blocking POSIX socket. */
int open_nb_socket(const char *addr, const char* port);

#endif /* ZOOQUITTO_UTILS_H */
