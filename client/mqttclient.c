#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mqtt.h>
#include <zookeeper.h>

#include "../lib/utils.h"

#define MCL_RANDOM_CHARS_LENGTH 21
#define ZOOQUITTO_TOPIC "zooquitto"
#define SEND_FREQ 20

/** MQTT client log alias - Adds `[MQTTCLIENT]` before the log. Logs to `stderr`. */
#define mcl_log(fmt, ...) fprintf(stderr, "[MQTTCLIENT] " fmt "\n" __VA_OPT__(,) __VA_ARGS__)

static const char alphabet[64] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_.";

/** Client state. */
struct client {
    const char *my_name;           /**< The name of this client. */
    clientid_t zk_id;              /**< ZooKeeper session ID. */
    zhandle_t *zk;                 /**< ZooKeeper handle. */
    struct mqtt_client mqtt;       /**< MQTT-C client. */
    int mqtt_sock;                 /**< MQTT socket. */
    pthread_t mqtt_pinger;         /**< MQTT refresher thread. */
    uint8_t sendbuf[2048];         /**< MQTT send buffer. */
    uint8_t recvbuf[1024];         /**< MQTT receive buffer. */
    ssize_t last_peer;             /**< Last peer with attempted usage. */
    bool zk_connected : 1;         /**< `true` = connection to ZooKeeper is active. */
    bool mqtt_inited : 1;          /**< `true` = `mqtt` has been initialized. */
    bool mqtt_pinger_active : 1;   /**< `true` = `mqtt_pinger` is active. */
};

uint64_t clock_millis(clockid_t clock)
{
    struct timespec ts;
    clock_gettime(clock, &ts);
    uint64_t result = ts.tv_nsec / 1000000;
    result += ts.tv_sec * 1000;
    return result;
}

void mcl_random_chars(char (*result)[MCL_RANDOM_CHARS_LENGTH])
{
    for (int i = 0; i < (MCL_RANDOM_CHARS_LENGTH - 1); i++)
        (*result)[i] = alphabet[rand() % sizeof(alphabet)];
    (*result)[MCL_RANDOM_CHARS_LENGTH - 1] = '\0';
}

void mcl_kill_mqtt(struct client *c)
{
    if (c->mqtt_inited)
        mqtt_disconnect(&c->mqtt);
    if (c->mqtt_sock >= 0) {
        close(c->mqtt_sock);
        c->mqtt_sock = -1;
    }
    if (c->mqtt_pinger_active) {
        pthread_cancel(c->mqtt_pinger);
        c->mqtt_pinger_active = false;
    }
}

void mcl_find_broker(struct client *c); /* Forward decl */

void *mcl_pinger(void *context)
{
    int runs = rand() % SEND_FREQ;
    struct client *c = (struct client *) context;
    while (true) {
        int ret = mqtt_sync(&c->mqtt);
        if (ret != MQTT_OK) {
            mcl_log("Broker lost, finding a new one...");
            c->last_peer += 1;
            mcl_find_broker(c);
            return NULL;
        }

        if ((runs % SEND_FREQ) == 0) {
            runs = 0;
            char genstr[MCL_RANDOM_CHARS_LENGTH];
            mcl_random_chars(&genstr);

            char *msg = NULL;
            asprintf(&msg, "%s : %s", c->my_name, genstr);
            if (msg) {
                mqtt_publish(&c->mqtt, ZOOQUITTO_TOPIC, msg, strlen(msg), 0);
                mcl_log("SEND(%s): %s", ZOOQUITTO_TOPIC, msg);
                free(msg);
            } else {
                mcl_log("Could not asprintf message!");
            }
        }
        runs += 1;

        usleep(100000);
    }
    return NULL;
}

void mcl_publish_response_callback(void **state, struct mqtt_response_publish *publish)
{
    char *topic = strndup(publish->topic_name, publish->topic_name_size);
    char *message = strndup(publish->application_message, publish->application_message_size);
    mcl_log("RECV(%s): %s", topic, message);
    free(topic);
    free(message);
}

void mcl_single_peer_completion(int rc, const char *value, int value_len, const struct Stat *stat, const void *context)
{
    struct client *c = (struct client *) context;
    char *addr = strndup(value, value_len);

    c->mqtt_sock = open_nb_socket(addr, "1883");
    if (c->mqtt_sock < 0) {
        perror("Could not open socket");
        c->last_peer += 1;
        mcl_find_broker(c);
        return;
    }

    if (c->mqtt_inited) {
        mqtt_reinit(&c->mqtt, c->mqtt_sock,
                c->sendbuf, sizeof(c->sendbuf),
                c->recvbuf, sizeof(c->recvbuf));
    } else {
        mqtt_init(&c->mqtt, c->mqtt_sock,
                c->sendbuf, sizeof(c->sendbuf),
                c->recvbuf, sizeof(c->recvbuf),
                mcl_publish_response_callback);
        c->mqtt_inited = true;
    }

    mqtt_connect(&c->mqtt, NULL, NULL, NULL, 0, NULL, NULL, MQTT_CONNECT_CLEAN_SESSION, 400);
    if (c->mqtt.error != MQTT_OK) {
        mcl_log("MQTT init error: %s", mqtt_error_str(c->mqtt.error));
        c->last_peer += 1;
        mcl_find_broker(c);
        return;
    }

    mcl_log("Connected to broker @ %s", addr);

    if (pthread_create(&c->mqtt_pinger, NULL, mcl_pinger, c)) {
        mcl_log("Could not start MQTT pinger");
        mcl_kill_mqtt(c);
    }
    c->mqtt_pinger_active = true;

    mqtt_subscribe(&c->mqtt, ZOOQUITTO_TOPIC, 0);
}

void mcl_peers_completion(int rc, const struct String_vector *peers, const void *context)
{
    struct client *c = (struct client *) context;
    if (rc != ZOK) {
        mcl_log("Could not get /zooquitto, retrying...");
        usleep(1000000);
        int ret = zoo_aget_children(c->zk, "/zooquitto", false, mcl_peers_completion, c);
        if (ret)
            mcl_log("Could not get /zooquitto: %d", ret);
        return;
    }

    if (c->last_peer < 0)
        c->last_peer = rand();
    c->last_peer %= peers->count;

    char *node = NULL;
    asprintf(&node, "/zooquitto/%s", peers->data[c->last_peer]);
    int ret = zoo_aget(c->zk, node, false, mcl_single_peer_completion, c);

    if (node)
        free(node);
}

void mcl_find_broker(struct client *c)
{
    mcl_kill_mqtt(c);
    int ret = zoo_aget_children(c->zk, "/zooquitto", false, mcl_peers_completion, c);
    if (ret)
        mcl_log("Could not get /zooquitto: %d", ret);
}

/** Called on ZooKeeper session state change. */
void mcl_session_watcher(zhandle_t *zh, int type, int state, const char *path, void *context)
{
    struct client *c = context;
    if (!c->zk)
        c->zk = zh;

    if (type == ZOO_SESSION_EVENT) {
        if (state == ZOO_CONNECTED_STATE) {
            c->zk_connected = true;
            mcl_log("ZooKeeper connected");

            const clientid_t *id = zoo_client_id(zh);
            if (c->zk_id.client_id == 0 || c->zk_id.client_id != id->client_id) {
                c->zk_id = *id;
                mcl_log("Got new session id: %" PRIx64, c->zk_id.client_id);
            }

            mcl_find_broker(c);
        } else if (state == ZOO_NOTCONNECTED_STATE) {
            c->zk_connected = false;
            mcl_log("ZooKeeper not connected");
        } else if (state == ZOO_AUTH_FAILED_STATE) {
            mcl_log("ZooKeeper authentication failure");
            zookeeper_close(zh);
            c->zk = NULL;
        } else if (state == ZOO_EXPIRED_SESSION_STATE) {
            mcl_log("ZooKeeper session expired");
            zookeeper_close(zh);
            c->zk = NULL;
        } else {
            mcl_log("ZooKeeper gave a different state: %d", state);
        }
    } else {
        mcl_log("ZooKeeper gave a different event: %d", type);
    }
}

/** Program entry point. */
int main(int argc, char **argv)
{
    srand(clock_millis(CLOCK_MONOTONIC));

    struct client *c = ch_malloc(sizeof(struct client));
    *c = (struct client) {
        .my_name = getenv("CLIENT_NAME"),
        .zk = NULL,
        .mqtt_sock = -1,
        .last_peer = -1,
        .zk_connected = false,
        .mqtt_inited = false,
    };

    c->zk = zookeeper_init(getenv("ZOOS"), mcl_session_watcher, 10000, 0, c, 0);
    if (!c->zk) {
        int err = errno;
        mcl_log("ZooKeeper initialization error: %s", strerror(err));
        free(c);
        return err;
    } else {
        mcl_log("ZooKeeper initialized");
    }

    bool running = true;
    while (running) {
        running = !usleep(100000);
    }

    if (c->zk)
        zookeeper_close(c->zk);

    free(c);
    return 0;
}
